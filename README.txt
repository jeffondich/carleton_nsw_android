Carleton New Student Week App

1. Background

This project began as a final project in Carleton College's
Spring 2014 class, CS342 Mobile Application Development, taught
by Professor Jeff Ondich. The project was proposed and guided by
Associate Dean of the College Louis Newman and Director of Student
Activities Lee Clark. The intent was to provide a mobile app
for first-year students to use to keep track of events, locations,
and vocabulary during Carleton's New Student Week.

During CS342, the team of Stephen Grinich '16, Evan Harris '15,
and Alex Simonides '15 developed a version of the app for iOS.
Grinich was then hired for the summer to polish the iOS app
and to port it to Android. Both apps were deployed during NSW '14.

Grinich developed the app using Eclipse. During spring 2015,
Ondich moved Grinich's sources to an Android Studio project to
enable students doing other projects in the 2015 version of CS342
to borrow from the NSW app. No effort was made to update or
modify the original app--just to get it to build and run.

2. Installation

Nearly everything you need should be here. As always, you may need to
modify SDK and version numbers in the build.gradle files, depending
on what you have installed.

There's one dependency that you will need to add: the Google
Play Services SDK.

(i) Launch Android Studio.
(ii) Go to Tools -> Android -> SDK Manager.
(iii) In SDK Manager, scroll down to Extras/Google Play Services
and select it.
(iv) Click on the "Install packages" button.

More information:
http://developer.android.com/google/play-services/setup.html


